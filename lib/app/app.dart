import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:device_info_plus/device_info_plus.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/material.dart';

import '../view/home.dart';
import '../view/webview.dart';

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final remoteConfig = FirebaseRemoteConfig.instance;
  String? appUrl;

  bool select = true;
  Future<void> checkDeviceAndUrl() async {
    appUrl = remoteConfig.getString('URL');
    // log('appUrl: $appUrl');
    bool isGoogleOrEmulator = await isGoogleDeviceOrEmulator();
    if (appUrl!.isEmpty || isGoogleOrEmulator) {
      setState(() {
        select = true;
      });
    } else {
      setState(() {
        select = false;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    checkDeviceAndUrl();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: select == null
          ? CircularProgressIndicator()
          : select
              ? const DiceApp(title: 'Demo version')
              : WebPage(
                  app_url: appUrl ?? '',
                ),
    );
  }
}

Future<bool> isGoogleDeviceOrEmulator() async {
  final DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
  if (Platform.isAndroid) {
    final AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
    return androidInfo.model.toLowerCase().contains('android sdk') ||
        androidInfo.manufacturer == 'Genymotion';
  }

  return false;
}
