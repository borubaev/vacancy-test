import 'package:flutter/material.dart';
import 'dart:math';

class DiceApp extends StatefulWidget {
  const DiceApp({super.key, required this.title});

  final String title;

  @override
  State<DiceApp> createState() => DiceGame();
}

class DiceGame extends State<DiceApp> {
  int leftdice = 1;
  int rightdice = 2;
  bool reverse = true;
  String text = '';

  void roll() {
    setState(() {
      leftdice = Random().nextInt(6) + 1;
      rightdice = Random().nextInt(6) + 1;
      text = leftdice > rightdice
          ? 'Player 1 wins'
          : leftdice == rightdice
              ? 'It turned out to be a draw'
              : 'Player 2 wins';
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Dice App'),
        centerTitle: true,
        elevation: 0,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              '$text',
              style: Theme.of(context).textTheme.headlineSmall,
            ),
            Row(
              children: [
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        alignment: Alignment.center,
                        height: 200,
                        child: Image.asset(
                          'assets/images/$leftdice.png',
                          width: 100,
                          height: 100,
                        ),
                      ),
                      ElevatedButton(
                        onPressed: reverse
                            ? () {
                                roll();
                                setState(() {
                                  reverse = false;
                                });
                              }
                            : null,
                        child: Text('Player 1'),
                      )
                    ],
                  ),
                ),
                const SizedBox(
                  width: 30,
                ),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        alignment: Alignment.center,
                        height: 200,
                        child: Image.asset(
                          'assets/images/$rightdice.png',
                          width: 100,
                          height: 100,
                        ),
                      ),
                      ElevatedButton(
                          onPressed: !reverse
                              ? () {
                                  roll();
                                  setState(() {
                                    reverse = true;
                                  });
                                }
                              : null,
                          child: Text('Player 2'))
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
