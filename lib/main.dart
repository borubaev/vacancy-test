import 'dart:convert';

import 'package:dice_app/app/app.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/services.dart';
import 'firebase_options.dart';

Future<void> loadJsonToRemoteConfig() async {
  final remoteConfig = FirebaseRemoteConfig.instance;
  await remoteConfig.setConfigSettings(RemoteConfigSettings(
    fetchTimeout: const Duration(seconds: 10),
    minimumFetchInterval: const Duration(minutes: 1),
  ));
  await remoteConfig.fetchAndActivate();
  final jsonString = await rootBundle.loadString('assets/json/config.json');
  final jsonMap = json.decode(jsonString);

  jsonMap.forEach((key, value) {
    remoteConfig.setDefaults({key: value.toString()});
  });
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  loadJsonToRemoteConfig();

  runApp(MyApp());
}
